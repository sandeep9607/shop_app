import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app2/providers/products.dart';
import 'package:shop_app2/widgets/app_drawer.dart';
import 'package:shop_app2/widgets/user_prod_item.dart';

import 'edit_product_screen.dart';

class UserProducts extends StatelessWidget {
  static const routeName = 'user_product';

  @override
  Widget build(BuildContext context) {
    final prodData = Provider.of<Products>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Product list'),
        actions: [
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () {
              Navigator.pushNamed(context, EditProductScreen.routeName);
            },
          )
        ],
      ),
      drawer: AppDrawer(),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: ListView.builder(
          itemCount: prodData.items.length,
          itemBuilder: (_, index) {
            return Column(
              children: [
                UserProdItem(
                  id: prodData.items[index].id,
                  title: prodData.items[index].title,
                  imgUrl: prodData.items[index].imageUrl,
                ),
                Divider(),
              ],
            );
          },
        ),
      ),
    );
  }
}
