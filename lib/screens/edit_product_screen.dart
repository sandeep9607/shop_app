import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app2/providers/product.dart';
import 'package:shop_app2/providers/products.dart';

class EditProductScreen extends StatefulWidget {
  static const routeName = 'edit_product';
  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _imageUrlFocusNode = FocusNode();
  final _imageUrlController = TextEditingController();
  final _form = GlobalKey<FormState>();

  var _isInit = true;
  var _isLoading = false;
  var product = Product(
      id: null, title: null, description: null, price: null, imageUrl: null);
  var _initProduct = {
    'title': '',
    'description': '',
    'price': '',
  };

  @override
  void initState() {
    super.initState();
    _imageUrlFocusNode.addListener(updateImageUrl);
  }

  @override
  void dispose() {
    super.dispose();
    _imageUrlFocusNode.removeListener(updateImageUrl);
    _priceFocusNode.dispose();
    _descriptionFocusNode.dispose();
    _imageUrlFocusNode.dispose();
    _imageUrlController.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isInit) {
      final productId = ModalRoute.of(context).settings.arguments as String;
      if (productId != null) {
        product =
            Provider.of<Products>(context, listen: false).findById(productId);
        _initProduct = {
          'title': product.title,
          'description': product.description,
          'price': product.price.toString(),
          // 'imageUrl': product.imageUrl,
        };
        _imageUrlController.text = product.imageUrl;
      }
    }
    _isInit = false;
  }

  void updateImageUrl() {
    if (!_imageUrlFocusNode.hasFocus) {
      setState(() {});
    }
  }

  void saveForm() {
    final isValid = _form.currentState.validate();
    if (!isValid) return;
    setState(() {
      _isLoading = true;
    });
    _form.currentState.save();
    //update product
    if (product.id != null) {
      Provider.of<Products>(context, listen: false)
          .updateProduct(product.id, product);
      setState(() {
        _isLoading = false;
      });
      Navigator.pop(context);
    } else {
      //save product
      Provider.of<Products>(context, listen: false)
          .addProduct(product)
          .catchError((error) {
        showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
                  title: Text('An error occured'),
                  content: Text('Something went wrong'),
                  actions: [
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: Text('Okay'),
                    ),
                  ],
                ));
      }).then(
        (_) {
          setState(() {
            _isLoading = false;
          });
          Navigator.pop(context);
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('New Product'),
        actions: [
          TextButton(
            onPressed: () {
              saveForm();
            },
            child: Text(
              'Save',
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                  key: _form,
                  child: ListView(
                    children: [
                      TextFormField(
                        initialValue: _initProduct['title'],
                        decoration: InputDecoration(labelText: 'Title'),
                        textInputAction: TextInputAction.next,
                        onSaved: (value) {
                          product = Product(
                            title: value,
                            description: product.description,
                            price: product.price,
                            imageUrl: product.imageUrl,
                            id: product.id,
                            isFavorite: product.isFavorite,
                          );
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please provide value';
                          }
                          return null;
                        },
                        onFieldSubmitted: (_) {
                          FocusScope.of(context).requestFocus(_priceFocusNode);
                        },
                      ),
                      TextFormField(
                        initialValue: _initProduct['price'],
                        decoration: InputDecoration(labelText: 'Price'),
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.number,
                        focusNode: _priceFocusNode,
                        onSaved: (value) {
                          product = Product(
                            title: product.title,
                            description: product.description,
                            price: double.parse(value),
                            imageUrl: product.imageUrl,
                            id: product.id,
                            isFavorite: product.isFavorite,
                          );
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter price';
                          }
                          if (double.tryParse(value) == null) {
                            return 'Please enter valid price';
                          }
                          if (double.parse(value) <= 0) {
                            return 'Price must be greater than zero';
                          }
                          return null;
                        },
                        onFieldSubmitted: (_) {
                          FocusScope.of(context)
                              .requestFocus(_descriptionFocusNode);
                        },
                      ),
                      TextFormField(
                        initialValue: _initProduct['description'],
                        decoration: InputDecoration(labelText: 'Description'),
                        maxLines: 3,
                        keyboardType: TextInputType.multiline,
                        focusNode: _descriptionFocusNode,
                        onSaved: (value) {
                          product = Product(
                            title: product.title,
                            description: value,
                            price: product.price,
                            imageUrl: product.imageUrl,
                            id: product.id,
                            isFavorite: product.isFavorite,
                          );
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter description';
                          }
                          if (value.length < 10) {
                            return 'Description must more than 10 character';
                          }
                          return null;
                        },
                        onFieldSubmitted: (_) {
                          FocusScope.of(context)
                              .requestFocus(_imageUrlFocusNode);
                        },
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Container(
                            width: 100,
                            height: 100,
                            margin: const EdgeInsets.only(top: 8, right: 10),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1, color: Colors.grey),
                            ),
                            child: _imageUrlController.text.isEmpty
                                ? Text('No Image')
                                : FittedBox(
                                    child:
                                        Image.network(_imageUrlController.text),
                                  ),
                          ),
                          Expanded(
                            child: TextFormField(
                              decoration:
                                  InputDecoration(labelText: 'Image Url'),
                              keyboardType: TextInputType.url,
                              focusNode: _imageUrlFocusNode,
                              controller: _imageUrlController,
                              onSaved: (value) {
                                product = Product(
                                  title: product.title,
                                  description: product.description,
                                  price: product.price,
                                  imageUrl: value,
                                  id: product.id,
                                  isFavorite: product.isFavorite,
                                );
                              },
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter image url';
                                }
                                if (!value.startsWith('http') &&
                                    !value.startsWith('https')) {
                                  return 'Please enter valid image url';
                                }
                                return null;
                              },
                              onFieldSubmitted: (_) {
                                saveForm();
                              },
                            ),
                          )
                        ],
                      )
                    ],
                  )),
            ),
    );
  }
}
