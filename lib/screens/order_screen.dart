import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app2/providers/orders.dart';
import 'package:shop_app2/widgets/app_drawer.dart';
import 'package:shop_app2/widgets/order_item_widget.dart';

class OrderScreen extends StatelessWidget {
  static const routeName = 'order_screen';
  @override
  Widget build(BuildContext context) {
    final ordersData = Provider.of<Orders>(context);

    return Scaffold(
      appBar: AppBar(title: Text('Your Orders')),
      drawer: AppDrawer(),
      body: ordersData.orders.length == 0
          ? Center(
              child: Text('No Orders Found! Please do some shopping'),
            )
          : ListView.builder(
              itemCount: ordersData.orders.length,
              itemBuilder: (context, index) => OrderItemWidget(
                order: ordersData.orders[index],
              ),
            ),
    );
  }
}
