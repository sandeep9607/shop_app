import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:shop_app2/providers/cart.dart';
import 'package:shop_app2/providers/products.dart';
import 'package:shop_app2/screens/cart_screen.dart';
import 'package:shop_app2/screens/edit_product_screen.dart';
import 'package:shop_app2/widgets/app_drawer.dart';
import 'package:shop_app2/widgets/badge.dart';
import 'package:shop_app2/widgets/product_item.dart';

enum FilterOption { Favorite, All }

class ProductsOverviewScreen extends StatefulWidget {
  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  var isFavorite = false;
  var isInit = true;
  @override
  void initState() {
    super.initState();
    // its hack but it is very useful if you want to use ModelRoute Data or .of(context)

    // Future.delayed(Duration.zero).then((_) {
    //   Provider.of<Products>(context).fetchProducts();
    // });
  }

  @override
  void didChangeDependencies() {
    if (isInit) {
      Provider.of<Products>(context).fetchProducts();
    }
    isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('MyShop'),
        actions: [
          PopupMenuButton(
              onSelected: (FilterOption selectedValue) {
                setState(() {
                  if (selectedValue == FilterOption.Favorite) {
                    isFavorite = true;
                  } else {
                    isFavorite = false;
                  }
                });
              },
              icon: Icon(Icons.more_vert),
              itemBuilder: (_) => [
                    PopupMenuItem(
                        child: Text('Favorite'), value: FilterOption.Favorite),
                    PopupMenuItem(
                        child: Text('Show All'), value: FilterOption.All)
                  ]),
          Consumer<Cart>(
            builder: (context, cart, ch) => Badge(
              child: ch,
              value: cart.itemCount.toString(),
            ),
            child: IconButton(
              icon: Icon(
                Icons.shopping_cart,
                color: Colors.yellowAccent,
              ),
              onPressed: () {
                Navigator.pushNamed(context, CartScreen.routeName);
              },
            ),
          )
        ],
      ),
      drawer: AppDrawer(),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.pushNamed(context, EditProductScreen.routeName);
        },
      ),
      body: ProductGrid(isFavorite: isFavorite),
    );
  }
}

class ProductGrid extends StatelessWidget {
  final bool isFavorite;

  const ProductGrid({Key key, this.isFavorite}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final productObj = Provider.of<Products>(context);
    final products = isFavorite ? productObj.favoriteItems : productObj.items;
    return GridView.builder(
      padding: const EdgeInsets.all(10),
      itemCount: products.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 3 / 2.5,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
      itemBuilder: (context, index) => ChangeNotifierProvider.value(
        //create: (context) => products[index],
        value: products[index],
        child: ProductItem(),
      ),
    );
  }
}
