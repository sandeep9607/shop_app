import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app2/providers/cart.dart';
import 'package:shop_app2/providers/product.dart';
import 'package:shop_app2/screens/product_detail.dart';

class ProductItem extends StatelessWidget {
  // final Product product;

  // ProductItem({this.product});
  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Product>(context, listen: false);
    final cart = Provider.of<Cart>(context, listen: false);
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, ProductDetail.routeName,
                arguments: product.id);
          },
          child: Image.network(
            product.imageUrl,
            fit: BoxFit.cover,
          ),
        ),
        header: GridTileBar(
          leading: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
              color: Colors.black54,
              borderRadius: BorderRadius.circular(20),
            ),
            child: Center(
              child: Text(
                '\$${product.price}',
                style: TextStyle(color: Colors.white, fontSize: 11),
              ),
            ),
          ),
        ),
        footer: GridTileBar(
          leading: IconButton(
            icon: Consumer<Product>(
              builder: (context, product, _) => Icon(
                product.isFavorite ? Icons.favorite : Icons.favorite_border,
                color: Colors.red,
              ),
            ),
            onPressed: () => product.toggleFavorite(),
          ),
          trailing: IconButton(
            icon: Icon(Icons.shopping_cart, color: Colors.yellow[800]),
            onPressed: () {
              cart.addItems(product.id, product.title, product.price);
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text('Item added to cart!'),
                action: SnackBarAction(
                  label: 'UNDO',
                  textColor: Colors.red,
                  onPressed: () {
                    // ... write code here to remove from cart
                    cart.removeRecentAdded(product.id);
                  },
                ),
              ));
            },
          ),
          backgroundColor: Colors.black87,
          title: Text(
            product.title,
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
